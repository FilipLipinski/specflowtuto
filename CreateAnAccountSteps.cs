﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecFlowTuto.Pages;

namespace SpecFlowTuto
{
    [Binding]
    public class CreateAnAccountSteps
    {
        private readonly PageData _pageData;

        public CreateAnAccountSteps(PageData pageData)
        {
            _pageData = pageData;
        }

        [Given(@"User go to PhPTravels homepage")]
        public void GivenUserGoToPhPTravelsHomepage()
        {
            Homepage homePage = new Homepage(new ChromeDriver(Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location))));
            homePage.SetupPageAndDriver();
            _pageData.Driver = homePage.Driver;
            _pageData.Page = homePage;
        }
        
        [When(@"User click on Sign Up button")]
        public void WhenUserClickOnSignUpButton()
        {
            var homePage = (Homepage) _pageData.Page;
            homePage
                .MyAccountSection
                .GoToRegistrationPage();
        }
        
        [Then(@"User is redirected to Sign Up page")]
        public void ThenUserIsRedirectedToSignUpPage()
        {
            var driver = _pageData.Driver;
            SignUpPage signUpPage = new SignUpPage(driver);
            Assert.IsTrue(signUpPage.IsEmailInputVisible && signUpPage.IsFirstNameInputVisible && signUpPage.IsLastNameInputVisible);
            Assert.IsTrue(signUpPage.PageUrl.Contains("/register"));
        }
    }
}
