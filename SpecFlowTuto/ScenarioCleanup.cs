﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpecFlowTuto.Pages;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using SpecFlowTuto.Pages.Sections;

namespace SpecFlowTuto
{
    [Binding]
    public sealed class ScenarioCleanup
    {
        private readonly PageData _pageData;

        public ScenarioCleanup(PageData pageData)
        {
            _pageData = pageData;
        }

        [AfterScenario]
        public void AfterScenario()
        {
            var driver = (IWebDriver) _pageData.Driver;
            driver.Quit();
            driver.Dispose();
        }

    }
}
