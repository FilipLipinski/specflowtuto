﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace SpecFlowTuto
{
    public abstract class BasePage : BaseDriver
    {
        public Uri PageUri { get; set; }
        public BasePage(IWebDriver driver) : base(driver)
        {
        }

        public void SetupPageAndDriver()
        {
            Driver.Navigate().GoToUrl(PageUri);
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            WaitForAjaxCallsToFinish();
        }

        public void RefreshPage()
        {
            Driver.Navigate().Refresh();
        }

        public void NavigateBack()
        {
            Driver.Navigate().Back();
        }

        public void NavigateForward()
        {
            Driver.Navigate().Forward();
        }
    }
}
