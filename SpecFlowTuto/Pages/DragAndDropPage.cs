﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace SpecFlowTuto.Pages
{
    class DragAndDropPage : BasePage
    {
        public DragAndDropPage(IWebDriver driver) : base(driver)
        {
            PageUri = new Uri("http://the-internet.herokuapp.com/drag_and_drop");
            Actions = new Actions(Driver);
        }

        private CustomWebElement BlockA => GetCustomElementByCss("#column-a");
        private CustomWebElement BlockB => GetCustomElementByCss("#column-b");
        private Actions Actions { get; set; }

        //following will not work on chrome
        public void DragAintoB()
        {
            Actions.DragAndDrop(BlockA.Element, BlockB.Element).Build().Perform();
        }

        public void DragAintoB2()
        {
            Actions.DragAndDropToOffset(BlockA.Element, 500, 0).Build().Perform();
        }

        public void DragAintoB3()
        {
            Actions.ClickAndHold(BlockA.Element).MoveToElement(BlockB.Element).Release(BlockA.Element).Build().Perform();
        }
    }
}
