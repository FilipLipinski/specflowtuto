﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using OpenQA.Selenium;

namespace SpecFlowTuto.Pages
{
    class BrowserTabsPage : BasePage
    {
        public BrowserTabsPage(IWebDriver driver) : base(driver)
        {
            PageUri = new Uri("http://the-internet.herokuapp.com/windows");
        }

        private CustomWebElement OpenNewTabButton => GetCustomElementByCss("#content a");
        private ReadOnlyCollection<string> WindowsCollection;

        public string OpenNewTab()
        {
            OpenNewTabButton.Element.Click();
            Thread.Sleep(TimeSpan.FromSeconds(3));
            WindowsCollection = Driver.WindowHandles;
            Driver.SwitchTo().Window(WindowsCollection[1]);
            return GetCustomElementByCss(".example h3").Element.Text;
        }

        public string SwitchToPreviousTab()
        {
            Driver.SwitchTo().Window(WindowsCollection[0]);
            return GetCustomElementByCss("#content h3").Element.Text;
        }
    }
}
