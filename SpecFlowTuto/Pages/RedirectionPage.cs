﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace SpecFlowTuto.Pages
{
    class RedirectionPage : BasePage
    {
        public RedirectionPage(IWebDriver driver) : base(driver)
        {
            PageUri = new Uri("http://the-internet.herokuapp.com/redirector");
        }

        private CustomWebElement RedirectionLabel => GetCustomElementByCss("#redirect");
        private CustomWebElement PageHeaderLabel => GetCustomElementByCss(".example h3");

        public string HeaderText => PageHeaderLabel.Element.Text;
        public void RedirectToStatusCodesPage()
        {
            RedirectionLabel.Element.Click();
        }
    }
}
