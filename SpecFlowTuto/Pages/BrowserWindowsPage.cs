﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace SpecFlowTuto.Pages
{
    class BrowserWindowsPage : BasePage
    {
        public BrowserWindowsPage(IWebDriver driver) : base(driver)
        {
            PageUri = new Uri("http://book.theautomatedtester.co.uk/chapter1");
        }

        private CustomWebElement OpenNewWindowLabel => GetCustomElementByCss("#multiplewindow.multiplewindow");
        private CustomWebElement NewWindowTextLabel => GetCustomElementByCss("#popuptext");
        private CustomWebElement CloseTheWindowLabel => GetCustomElementByCss("#closepopup");
        private CustomWebElement BasePageHeaderLabel => GetCustomElementByCss(".mainheading");
        private ReadOnlyCollection<string> WindowsCollection;

        public string NewWindowText => NewWindowTextLabel.Element.Text;
        public string BasePageHeaderText => BasePageHeaderLabel.Element.Text;

        public BrowserWindowsPage ClickToOpenNewWindow()
        {
            OpenNewWindowLabel.Element.Click();
            WindowsCollection = Driver.WindowHandles;
            Driver.SwitchTo().Window(WindowsCollection[1]);
            return this;
        }

        public BrowserWindowsPage CloseOpenedWindow()
        {
            CloseTheWindowLabel.Element.Click();
            Driver.SwitchTo().Window(WindowsCollection[0]);
            return this;
        }
    }
}
