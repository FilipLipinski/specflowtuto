﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using SpecFlowTuto.Pages.Sections;

namespace SpecFlowTuto.Pages
{
    class SignUpPage : BaseDriver
    {
        public MyAccountSection MyAccountSection;

        public SignUpPage(IWebDriver driver) : base(driver)
        {
            MyAccountSection = new MyAccountSection(driver);
            PageUrl = driver.Url;
        }

        private CustomWebElement FirstNameInput => GetCustomElementByCss("input[name='firstname']");
        private CustomWebElement LastNameInput => GetCustomElementByCss("input[name='lastname']");
        private CustomWebElement EmailInput => GetCustomElementByCss("input[name='email']");

        public bool IsFirstNameInputVisible => FirstNameInput.Element.Displayed;
        public bool IsLastNameInputVisible => LastNameInput.Element.Displayed;
        public bool IsEmailInputVisible => EmailInput.Element.Displayed;
        public string PageUrl { get; }


    }
}
