﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace SpecFlowTuto.Pages.Sections
{
    class MyAccountSection : BaseDriver
    {
        public MyAccountSection(IWebDriver driver) : base(driver)
        { }

        private CustomWebElement MyAccountHeaderMenuElement => GetCustomElementByCss(".dropdown-tab #dropdownCurrency");
        private CustomWebElement MyAccountHeaderLoginElement => FindInCustomWebElement(MyAccountHeaderMenuElement, ".dropdown-item.tr:first-of-type");
        private CustomWebElement MyAccountHeaderSignUpElement => FindInCustomWebElement(MyAccountHeaderMenuElement, ".dropdown-item.tr:last-of-type");

        private IWebElement SignUpElement =>
            Driver.FindElement(By.XPath("//a[contains(text(),'Sign Up') and contains(@class, 'tr')]"));

        public void GoToLoginPage()
        {
            ExpandMyAccountMenu();
            MyAccountHeaderLoginElement.Element.Click();
        }

        public void GoToRegistrationPage()
        {
            ExpandMyAccountMenu();
            //WaitForElement(MyAccountHeaderSignUpElement.ElementSelector);
            SignUpElement.Click();
        } 
        
        private void ExpandMyAccountMenu()
        {
            WaitForElement(MyAccountHeaderMenuElement.ElementSelector);
            MyAccountHeaderMenuElement.Element.Click();
            //WaitForElement(MyAccountHeaderLoginElement.ElementSelector);
        }
    }
}
