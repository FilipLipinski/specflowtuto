﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using SpecFlowTuto.Pages.Sections;

namespace SpecFlowTuto.Pages
{
    class Homepage : BasePage
    {
        public MyAccountSection MyAccountSection;

        public Homepage(IWebDriver driver) : base(driver)
        {
            PageUri = new Uri("https://www.phptravels.net/");
            MyAccountSection = new MyAccountSection(Driver);
        }

    }
}
