﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace SpecFlowTuto.Pages.Sections
{
    [Binding]
    public sealed class DragAndDropSteps
    {
        private readonly PageData _pageData;

        public DragAndDropSteps(PageData pageData)
        {
            _pageData = pageData;
        }

        [Given(@"I go drag and drop page")]
        public void GivenIGoDragAndDropPage()
        {
            DragAndDropPage dragAndDropPage = new DragAndDropPage(new ChromeDriver(Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location))));
            dragAndDropPage.SetupPageAndDriver();
            _pageData.Driver = dragAndDropPage.Driver;
            _pageData.Page = dragAndDropPage;
        }

        [When(@"I drag A box on B box")]
        public void WhenIDragABoxOnBBox()
        {
            var driver = _pageData.Driver;
            var page = (DragAndDropPage) _pageData.Page;
            page.DragAintoB();
            //Thread.Sleep(TimeSpan.FromSeconds(2));
        }


    }
}
