﻿using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using SpecFlowTuto.Pages;
using TechTalk.SpecFlow;

namespace SpecFlowTuto.Steps
{
    [Binding]
    class BrowserWindowsSteps
    {
        private readonly PageData _pageData;

        public BrowserWindowsSteps(PageData pageData)
        {
            _pageData = pageData;
        }

        [Given(@"I go to browser windows page")]
        public void GivenIGoToBrowserWindowsPage()
        {
            BrowserWindowsPage browserWindowsPage = new BrowserWindowsPage(new ChromeDriver(Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location))));
            browserWindowsPage.SetupPageAndDriver();
            _pageData.Driver = browserWindowsPage.Driver;
            _pageData.Page = browserWindowsPage;
        }

        [When(@"I click on open new window label")]
        public void WhenIClickOnOpenNewWindowLabel()
        {
            var page = (BrowserWindowsPage) _pageData.Page;
            page.ClickToOpenNewWindow();
        }

        [Then(@"I land on new browser window")]
        public void ThenILandOnNewBrowserWindow()
        {
            var page = (BrowserWindowsPage) _pageData.Page;
            Assert.IsTrue(page.NewWindowText.Contains("Text within the pop up window"));
        }

        [When(@"I close new browser window")]
        public void WhenICloseNewBrowserWindow()
        {
            var page = (BrowserWindowsPage) _pageData.Page;
            page.CloseOpenedWindow();
        }

        [Then(@"I land on base browser page")]
        public void ThenILandOnBaseBrowserPage()
        {
            var page = (BrowserWindowsPage) _pageData.Page;
            Assert.IsTrue(page.BasePageHeaderText.Contains("Selenium: Beginners Guide"));
        }

        [Given(@"I go to redirections page")]
        public void GivenIGoToRedirectionsPage()
        {
            RedirectionPage redirectionPage = new RedirectionPage(new ChromeDriver(Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location))));
            redirectionPage.SetupPageAndDriver();
            _pageData.Driver = redirectionPage.Driver;
            _pageData.Page = redirectionPage;
        }

        [When(@"I click on redirection button")]
        public void WhenIClickOnRedirectionButton()
        {
            var page = (RedirectionPage) _pageData.Page;
            page.RedirectToStatusCodesPage();
        }

        [Then(@"I will land on status codes page")]
        public void ThenIWillLandOnStatusCodesPage()
        {
            var page = (RedirectionPage)_pageData.Page;
            Assert.IsTrue(page.HeaderText.Contains("Status Codes"));
        }

        [When(@"I use browser navigation to go back")]
        public void WhenIUseBrowserNavigationToGoBack()
        {
            _pageData.Page.NavigateBack();
        }

        [When(@"I use brwoser navigation to go forward")]
        public void WhenIUseBrwoserNavigationToGoForward()
        {
            _pageData.Page.NavigateForward();
        }

        [Then(@"I will land on redirecations page")]
        public void ThenIWillLandOnRedirecationsPage()
        {
            var page = (RedirectionPage)_pageData.Page;
            Assert.IsTrue(page.HeaderText.Contains("Redirection"), $"Page header text doesn't match, current header text: {page.HeaderText}");
        }

    }
}
