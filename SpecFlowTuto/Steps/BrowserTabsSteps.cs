﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using SpecFlowTuto.Pages;
using TechTalk.SpecFlow;

namespace SpecFlowTuto.Steps
{
    public class CurrentTabText
    {
        public string TabText { get; set; }
    }

    [Binding]
    public class BrowserTabsSteps
    {
        private readonly PageData _pageData;
        private readonly CurrentTabText _currentTabText;

        public BrowserTabsSteps(PageData pageData, CurrentTabText currentTabText)
        {
            _pageData = pageData;
            _currentTabText = currentTabText;
        }

        [Given(@"I go to browser tabs page")]
        public void GivenIGoToBrowserTabsPage()
        {
            BrowserTabsPage browserTabsPage = new BrowserTabsPage(new ChromeDriver(Path.GetDirectoryName((Assembly.GetExecutingAssembly().Location))));
            browserTabsPage.SetupPageAndDriver();
            _pageData.Page = browserTabsPage;
            _pageData.Driver = browserTabsPage.Driver;
        }

        [When(@"I click on Click Here button")]
        public void WhenIClickOnClickHereButton()
        {
            var page = (BrowserTabsPage) _pageData.Page;
            _currentTabText.TabText = page.OpenNewTab();
        }

        [Then(@"I will land on new browser tab")]
        public void ThenIWillLandOnNewBrowserTab()
        {
            var page = (BrowserTabsPage) _pageData.Page;
            Assert.IsTrue(_currentTabText.TabText == "New Window");
        }

        [When(@"I switch to previous tab")]
        public void WhenISwitchToPreviousTab()
        {
            var page = (BrowserTabsPage) _pageData.Page;
            _currentTabText.TabText = page.SwitchToPreviousTab();
        }

        [Then(@"I will land on prevoius browser tab")]
        public void ThenIWillLandOnPrevoiusBrowserTab()
        {
            Assert.IsTrue(_currentTabText.TabText == "Opening a new window");
        }
    }
}
