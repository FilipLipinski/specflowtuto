﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SpecFlowTuto
{
    public abstract class BaseDriver
    {
        public IWebDriver Driver;
        private readonly int WaitTimeInSeconds = 5;

        protected BaseDriver(IWebDriver driver)
        {
            Driver = driver;
        }

        protected IWebElement FindByCss(string selector)
        {
            return Driver.FindElement(By.CssSelector(selector));
        }

        protected CustomWebElement GetCustomElementByCss(string selector)
        {
            return new CustomWebElement(selector, FindByCss(selector));
        }

        protected CustomWebElement FindInCustomWebElement(CustomWebElement element, string selector)
        {
            return new CustomWebElement(selector, element.Element.FindElement(By.CssSelector(selector)));
        }

        protected IWebElement WaitForElement(string elementSelector)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(WaitTimeInSeconds));
            return wait.Until(Driver => Driver.FindElement(By.CssSelector(elementSelector)));
        }

        public void WaitForAjaxCallsToFinish()
        {
            while (true) // add timeout
            {
                var ajaxIsComplete = (bool)(Driver as IJavaScriptExecutor).ExecuteScript("return jQuery.active == 0");
                if (ajaxIsComplete)
                    break;
                Thread.Sleep(100);
            }
        }
    }

    public class CustomWebElement
    {
        public string ElementSelector { get; set; }
        public IWebElement Element { get; set; }

        public CustomWebElement(string selector, IWebElement webElement)
        {
            ElementSelector = selector;
            Element = webElement;
        }
    }
}
