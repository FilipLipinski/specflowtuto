﻿Feature: BrowserWindows

Scenario: Switch between browser windows
	Given I go to browser windows page
	When I click on open new window label
	Then I land on new browser window
	When I close new browser window 
	Then I land on base browser page

Scenario: Use browser navigation
	Given I go to redirections page
	When I click on redirection button
	Then I will land on status codes page
	When I use browser navigation to go back
	Then I will land on redirecations page
	When I use brwoser navigation to go forward
	Then I will land on status codes page
