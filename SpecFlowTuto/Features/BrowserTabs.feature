﻿Feature: BrowserTabs

Scenario: Switch between browser tabs
	Given I go to browser tabs page
	When I click on Click Here button
	Then I will land on new browser tab
	When I switch to previous tab
	Then I will land on prevoius browser tab
